// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectUnknownGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTUNKNOWN_API AProjectUnknownGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
